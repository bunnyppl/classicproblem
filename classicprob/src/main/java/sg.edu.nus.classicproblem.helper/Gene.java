package sg.edu.nus.classicproblem.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;



public class Gene {
    public enum Nuclectide {
        A, C, G, T
    }
    private ArrayList<Codon> codons = new ArrayList<>();
    
    
    public Gene (String geneStr){
        for(int i = 0; i < geneStr.length() -3; i+=3){
            codons.add(new Codon(geneStr.substring(i, i +3)));
        }
    }

    public static class Codon implements Comparable<Codon> {
        public final Nuclectide first, second, third;
        private final Comparator<Codon> comparator = 
            Comparator.comparing((Codon c) -> c.first)
                      .thenComparing((Codon c) -> c.second)
                      .thenComparing((Codon c) -> c.third);

        public Codon(String codonStr){
            System.out.println(codonStr);
            first = Nuclectide.valueOf(codonStr.substring(0,1));
            second = Nuclectide.valueOf(codonStr.substring(1,2));
            third = Nuclectide.valueOf(codonStr.substring(2,3));
        }


        @Override
        public int compareTo(Codon other) {
            return comparator.compare(this, other);
        }
    }

    public boolean linearContains(Codon key){
        long start = System.nanoTime();
        for(Codon codon : codons){
            if(codon.compareTo(key) == 0){
                return true;
            }
        }
        long finished = System.nanoTime();
        long timeElapsed = finished - start;
        System.out.println(timeElapsed);
        return false;
    }

    public boolean binaryContains(Codon key){
        long start = System.nanoTime();
        ArrayList<Codon> sortedCodons = new ArrayList<>(codons);
        Collections.sort(sortedCodons);
        int low = 0;
        int high = sortedCodons.size() - 1;
        while ( low  <= high ){
            int middle = (low + high) / 2;
            int comparison = codons.get(middle).compareTo(key);
            if (comparison < 0) {
                low = middle + 1;
            }else if (comparison > 0){
                high  = middle -1;
            }else {
                return true;
            }
        }
        long finished = System.nanoTime();
        long timeElapsed = finished - start;
        System.out.println(timeElapsed);
        return false;
    }
}
